package com.lazday.javasqlite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lazday.javasqlite.database.NoteModel;

import java.util.List;

public class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {

    private List<NoteModel> listNote;
    private Context context;
    private OnAdapterListener listener;

    public NotesAdapter(Context context, List<NoteModel> listNote, OnAdapterListener listener) {
        this.context    = context ;
        this.listNote    = listNote ;
        this.listener   = listener ;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new ViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_notes,
                        parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder viewHolder, int position) {

        final NoteModel note = listNote.get(position);

        viewHolder.textTitle.setText( note.getTitle() );
        viewHolder.textNote.setText( note.getNote() );

        viewHolder.imageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDelete(note);
            }
        });

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick( note );
            }
        });
    }

    @Override
    public int getItemCount() {
        return listNote.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textTitle,textNote;
        ImageView imageDelete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_title);
            textNote = itemView.findViewById(R.id.text_note);
            imageDelete = itemView.findViewById(R.id.image_delete);
        }
    }

    public void submitList(List<NoteModel> notes){
        listNote.clear();
        listNote.addAll(notes);
        notifyDataSetChanged();
    }

    public interface OnAdapterListener {
        void onClick(NoteModel note);
        void onDelete(NoteModel note);
    }
}

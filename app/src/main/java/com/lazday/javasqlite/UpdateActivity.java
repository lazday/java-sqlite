package com.lazday.javasqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.lazday.javasqlite.database.DatabaseClient;
import com.lazday.javasqlite.database.DatabaseService;
import com.lazday.javasqlite.database.NoteDao;
import com.lazday.javasqlite.database.NoteModel;

public class UpdateActivity extends AppCompatActivity {

    private DatabaseService databaseService;
    private NoteDao noteDao;

    private NoteModel note;

    private EditText editTitle, editNote;
    private MaterialButton buttonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update);
        getSupportActionBar().setTitle("Edit Catatan");

        databaseService = new DatabaseClient(this).getService();
        noteDao = databaseService.noteDao();

        editTitle = findViewById(R.id.edit_title);
        editNote = findViewById(R.id.edit_note);
        buttonSave = findViewById(R.id.button_save);

        note = (NoteModel) getIntent().getSerializableExtra("extra_note");
        Log.e( "UpdateActivity", "extra_note : " + note.toString());

        editTitle.setText( note.getTitle() );
        editNote.setText( note.getNote() );
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NoteModel noteModel = new NoteModel();
                noteModel.setId( note.getId() );
                noteModel.setTitle( editTitle.getText().toString() );
                noteModel.setNote( editNote.getText().toString() );

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        noteDao.update( noteModel );
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(
                                        UpdateActivity.this,
                                        "Perubahan Disimpan",
                                        Toast.LENGTH_SHORT
                                ).show();
                                finish();
                            }
                        });
                    }
                }).start();

            }
        });
    }
}
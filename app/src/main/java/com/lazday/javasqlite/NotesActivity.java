package com.lazday.javasqlite;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.lazday.javasqlite.database.DatabaseClient;
import com.lazday.javasqlite.database.DatabaseService;
import com.lazday.javasqlite.database.NoteDao;
import com.lazday.javasqlite.database.NoteModel;

import java.util.ArrayList;
import java.util.List;

public class NotesActivity extends AppCompatActivity {

    private DatabaseService databaseService;
    private NoteDao noteDao;

    private List<NoteModel> notes;
    private NotesAdapter notesAdapter;

    private MaterialButton buttonNote;
    private RecyclerView listNote;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        databaseService = new DatabaseClient(this).getService();
        noteDao = databaseService.noteDao();

        buttonNote = findViewById(R.id.button_note);
        listNote = findViewById(R.id.list_note);

        notes = new ArrayList<>();
        notesAdapter = new NotesAdapter(this, notes, new NotesAdapter.OnAdapterListener() {

            @Override
            public void onClick(NoteModel note) {
                Intent intent = new Intent(NotesActivity.this, UpdateActivity.class);
                intent.putExtra("extra_note", note);
                startActivity(intent);
            }

            @Override
            public void onDelete(NoteModel note) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        noteDao.delete( note );
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(
                                        NotesActivity.this,
                                        "Catatan Dihapus",
                                        Toast.LENGTH_SHORT
                                ).show();
                                getNotes();
                            }
                        });
                    }
                }).start();
            }
        });
        listNote.setAdapter( notesAdapter );

        buttonNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NotesActivity.this, CreateActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        getNotes();
    }

    private void getNotes(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                List<NoteModel> results = noteDao.notes();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e( "NotesActivity", "getNotes : " + results.toString());
                        notes = results;
                        notesAdapter.submitList( notes );
                    }
                });
            }
        }).start();
    }
}
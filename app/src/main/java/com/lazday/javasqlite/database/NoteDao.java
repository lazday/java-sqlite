package com.lazday.javasqlite.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void create(NoteModel itemModel);

    @Update
    void update(NoteModel productModel);

    @Delete
    void delete(NoteModel productModel);

    @Query("SELECT * FROM NoteModel")
    List<NoteModel> notes();

}

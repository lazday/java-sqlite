package com.lazday.javasqlite.database;

import android.content.Context;

import androidx.room.Room;

public class DatabaseClient {

    private Context context;
    private static DatabaseClient client;
    private DatabaseService service;

    public DatabaseClient(Context context) {
        this.context = context;
        service = Room.databaseBuilder(
                context,
                DatabaseService.class,
                "notes1.db"
        )
                .fallbackToDestructiveMigration()
                .build();
    }

    public static synchronized DatabaseClient getInstance(Context context) {
        if (client == null) {
            client = new DatabaseClient(context);
        }
        return client;
    }

    public DatabaseService getService() {
        return service;
    }

}

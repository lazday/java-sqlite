package com.lazday.javasqlite.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(
        entities = {NoteModel.class},
        version = 1,
        exportSchema = false
)
public abstract class DatabaseService extends RoomDatabase {
    public abstract NoteDao noteDao();
}


package com.lazday.javasqlite;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.button.MaterialButton;
import com.lazday.javasqlite.database.DatabaseClient;
import com.lazday.javasqlite.database.DatabaseService;
import com.lazday.javasqlite.database.NoteDao;
import com.lazday.javasqlite.database.NoteModel;

public class CreateActivity extends AppCompatActivity {

    private DatabaseService databaseService;
    private NoteDao noteDao;

    private EditText editTitle, editNote;
    private MaterialButton buttonSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        getSupportActionBar().setTitle("Tulis Catatan");

        databaseService = new DatabaseClient(this).getService();
        noteDao = databaseService.noteDao();

        editTitle = findViewById(R.id.edit_title);
        editNote = findViewById(R.id.edit_note);
        buttonSave = findViewById(R.id.button_save);

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isRequire()) {

                    NoteModel noteModel = new NoteModel();
                    noteModel.setTitle( editTitle.getText().toString() );
                    noteModel.setNote( editNote.getText().toString() );

                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            noteDao.create( noteModel );
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(
                                            CreateActivity.this,
                                            "Catatan Disimpan",
                                            Toast.LENGTH_SHORT
                                    ).show();
                                    finish();
                                }
                            });
                        }
                    }).start();
                }
            }
        });

    }

    private Boolean isRequire(){

        if (editTitle.getText().toString().isEmpty()) {
            editTitle.setError("Judul tidak boleh kosong");
            return false;
        } else if (editNote.getText().toString().isEmpty()) {
            editNote.setError("Catatan tidak boleh kosong");
            return false;
        }

        return true;
    }
}